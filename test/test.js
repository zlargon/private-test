var expect = require('chai').expect;

describe('test', function() {

  it('1 == 1', function() {
    expect(1).to.equal(1);
  });

  it('2 == 2', function() {
    expect(2).to.equal(2);
  });

});
